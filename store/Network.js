import { observable, action } from 'mobx';
const axios = require('axios');

const data = [
    {
        url: 'https://api.tronalddump.io/random/quote',
        name: 'Donald Trump',
        textColor: '#e7dcca',
        backgroundColor: '#08449a',
        value: ''
    },
    {
        url: 'https://api.chucknorris.io/jokes/random',
        name: 'Chuck Norris',
        textColor: '#031634',
        backgroundColor: '#fe9b35',
        value: ''
    }
]

class Network {
    @observable content = {};

    //get 50/50 chance
    getRandom = () => Math.floor((Math.random() * 2) + 1);

    async initializeNetwork() {
        const index = this.getRandom() - 1;//index must start with 0
        const url = data[index].url;

        try {
            const result = await axios.get(url);// Make a request for a qoutes
            data[index].value =  result.data.value;
            this.content = data[index];
        } catch (error) {
            console.warn('err ', error)
        }

    }
}

const net = new Network();
export default net;
