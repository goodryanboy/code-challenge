import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import { StyleSheet } from 'react-native'

export default class ButtonComponent extends React.PureComponent {
    render() {
        const { onPress, title } = this.props;
        return (
            <TouchableOpacity 
                onPress={ onPress } 
                style={ style.touchableStyle }>
                <Text style={ style.textStyle }>{title}</Text>
            </TouchableOpacity>
        )
    }
}

const style = StyleSheet.create({
    touchableStyle: {
        backgroundColor: '#036564',
        paddingVertical: 12,
        width: '100%',
        alignItems: 'center'
    },
    textStyle: {
        color: '#e9dcca',
        fontSize: 26,
        fontFamily: 'sans-serif-black'
    }
})