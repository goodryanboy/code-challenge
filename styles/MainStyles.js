import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    mainScreen: {
        flex: 1,
        backgroundColor: '#031634',
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainScreenText: {
        color: '#c4b389',
        fontSize: 28,
        fontFamily: 'sans-serif-black'
    },
    contentJustify: {
        flex: 1,
        marginTop: '43%'
    },
    mainScreenButtonContainer: { 
        width: '100%', 
        paddingHorizontal: 28, 
        justifyContent: 'flex-end', 
        paddingBottom: '24%'
    },
    factScreen: {
        flex: 1,
        backgroundColor: '#e8ddcb',
    },
    factScreenHeader: {
        backgroundColor: '#fe9b35',
        paddingTop: 22, 
        paddingBottom: 10, 
        alignItems: 'center'
    },
    factScreenContainer: {
        flex: 1, 
        justifyContent: 'space-between', 
        paddingTop: '12%', 
        paddingHorizontal: 27, 
        paddingBottom: '2%'
    },
    factScreenTextContainer: {
        color: '#415a5d', 
        fontSize: 20, 
        lineHeight: 28,
        fontFamily: 'sans-serif-light'
    },
    headerText: {
        fontSize: 26, 
        fontWeight: 'bold', 
        color: '#031634',
        fontFamily: 'sans-serif-light'
    }
})

export default styles