/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react'
import { Provider } from 'mobx-react/native'
import { createAppContainer ,createSwitchNavigator } from "react-navigation"

import MainScreen from './screens/MainScreen'
import FactScreen from './screens/FactScreen'
import Network from './store/Network'

const Navigator = new createSwitchNavigator({
  MainScreen: {
    screen: MainScreen
  },
  FactScreen: {
    screen: FactScreen
  },
},{
  initialRouteName: 'MainScreen',
  backBehavior: true,
})

const AppMain = createAppContainer(Navigator)

export default class App extends Component {
  render() {
    return (
      <Provider network={Network} >
        <AppMain />
      </Provider>
    );
  }
}