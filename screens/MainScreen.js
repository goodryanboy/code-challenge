import React from 'react'
import { View, Text } from 'react-native'

import Button from '../components/Button'
import style from '../styles/MainStyles'

const Title = () => (
    <View style={{alignItems: 'center'}}>
        <Text style={ style.mainScreenText }>Chuck & Donald</Text>
        <Text style={ style.mainScreenText }>Facts & Qoutes</Text>
    </View>
)


export default class MainScreen extends React.Component {

    navigate = () => this.props.navigation.navigate('FactScreen');

    render(){
        return (
            <View style={ style.mainScreen }>
                <View style={ style.contentJustify }>
                    <Title />
                </View>
                <View style={style.mainScreenButtonContainer}>
                    <Button title="START" onPress={this.navigate} />
                </View>
            </View>
        )
    }
}