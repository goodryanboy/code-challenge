import React from 'react'
import { View, Text } from 'react-native'
import { observer, inject } from 'mobx-react/native'

import Button from '../components/Button'
import style from '../styles/MainStyles'

const Header = ({ text, textColor, backgroundColor }) => (
    <View style={[ style.factScreenHeader, { backgroundColor }]}>
        <Text style={[ style.headerText, { color: textColor} ]}>{ text }</Text>
    </View>
)

@inject('network') @observer
export default class FactScreen extends React.Component {

    componentDidMount() {
        this.oneMore();
    }

    oneMore = () => this.props.network.initializeNetwork();

    render(){
        const { content } = this.props.network;
        
        return (
            <View style={style.factScreen}>
                <Header 
                    text={ content.name } 
                    textColor={ content.textColor } 
                    backgroundColor={ content.backgroundColor }    
                />
                <View style={style.factScreenContainer}>
                    <Text style={style.factScreenTextContainer}>"{ content.value }"</Text>
                    <Button title="ONE MORE" onPress={ this.oneMore }/>
                </View>
            </View>
        )
    }
}